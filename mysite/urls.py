from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^admin/', include(admin.site.urls)),
    url(r'^classify/',include('learner.url')),
    url(r'^rank/(?P<symptoms>.[() a-zA-Z0-9,]+)/$','learner.views.rank'),
    url(r'^add/',include('learner.add')),
    url(r'^getlist/','learner.views.getlist'),
    url(r'^classifiedlist/','learner.views.classifiedlist'),
    url(r'^train/','learner.views.train'),
    url(r'^dlist/','learner.views.getdiseaselist'),
    url(r'^intro/(?P<Disease>.[() a-zA-Z0-9,]+)/$','learner.views.getIntro'),
    url(r'^cause/(?P<Disease>.[() a-zA-Z0-9,]+)/$','learner.views.getCause'),
    url(r'^symptoms/(?P<Disease>.[() a-zA-Z0-9,]+)/$','learner.views.getSymptoms'),
    url(r'^prevention/(?P<Disease>.[() a-zA-Z0-9,]+)/$','learner.views.getPrevention'),
    url(r'^treatment/(?P<Disease>.[() a-zA-Z0-9,]+)/$','learner.views.getTreatment'),
    url(r'^seedoc/(?P<Disease>.[() a-zA-Z0-9,]+)/$','learner.views.getSeedoc'),

)
