from learner.models import *
from learner.algorithm import *
from collections import Counter
def viewtraindb(db):
	symptoms = map(int,db.symptoms.split(','))
	print "disease >> "+diseaseDB.objects.filter(id=db.disease)[0].disease
	print "symptoms >>"
	for sym in symptoms:
		print symptomsDB.objects.filter(id=sym)[0].symptom
	print "\n\n"



def createDataSet():
	labels = [s.symptom for s in symptomsDB.objects.all()]
	labelscount = len(symptomsDB.objects.all())
	data = []
	for trainsets in trainDB.objects.all():
		symptoms = map(int,trainsets.symptoms.split(','))
		temp = [0]*labelscount
		for index in symptoms:
			temp[index-1] = 1
		temp.append(diseaseDB.objects.filter(id=trainsets.disease)[0].disease)
		data.append(temp)
	return data,labels

def getlabels():
	return [s.symptom for s in symptomsDB.objects.all()]

def cusclassify(symptoms):
	symptoms = map(int,symptoms)
	labels = [s.symptom for s in symptomsDB.objects.all()]
	labelscount = len(symptomsDB.objects.all())
	data = [0]*labelscount
	for index in symptoms:
		data[index-1] = 1
	Tree = retriveTree()
	return classify(Tree,labels,data)

def pushNewTrainData(symptom,diseaseno):
	data = ''
	for index in symptom:
		data+=str(index)+','
	data = data[:-1]
	newdata = trainDB(symptoms=data,disease=diseaseno)
	newdata.save()

def getdiseaselist():
	return [s.disease for s in diseaseDB.objects.all()]

def match(sel,orig):
	per = Counter([sym in orig for sym in sel])
	if not True in per: return 0.0
	else: return float(per[True])/len(orig)*100



def rank(symptoms):#this will return top 3 hits based on matched symptoms
	import operator
	symptoms = map(int,symptoms)
	matcher = [map(int,sym.symptoms.split(',')) for sym in diseaseDB.objects.all()]
	matchedper = [match(symptoms,orig) for orig in matcher]
	for i in range(len(matcher)):
		matchedper[i] = (i+1,matchedper[i])

	matchedper = sorted(matchedper,key=lambda x:x[1],reverse=True)[:3]
	final = '('
	for (index,per) in matchedper:
		final += str(diseaseDB.objects.filter(id=index)[0].disease)+','+str(per)+'|'
	final = final[:-1]+')'
	return final

def classifiedlist():
	import operator
	labels = getlabels()
	sym = [(operator.itemgetter(*tuple([i-1 for i in map(int,objects.symptoms.split(','))]))(labels),objects.head) for objects in classifyDB.objects.all()]
	final = '('
	for slist,listname in sym:
		if type(slist)==tuple:
			for symp in slist:
				final += str(symp)+','
			final = final[:-1]+'>'+listname+'|'
		else:
			final += str(slist)+'>'+listname+'|'
	final = final[:-1]+')'
	return str(final)