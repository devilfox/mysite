-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 12, 2014 at 08:25 AM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_myguide`
--

-- --------------------------------------------------------

--
-- Table structure for table `learner_diseasedb`
--

CREATE TABLE IF NOT EXISTS `learner_diseasedb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symptoms` longtext NOT NULL,
  `disease` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `learner_diseasedb`
--

INSERT INTO `learner_diseasedb` (`id`, `symptoms`, `disease`) VALUES
(1, '1,2,11,3,4,13,19,20', 'common cold'),
(2, '11,13,63,3,5', 'chicken pox'),
(3, '21,12,1,2,11,3,4,13,19,20,6,8,14,15', 'ear infection'),
(4, '2,3,9,11,22,63,35', 'sinus infection'),
(5, '3,12,22,15,14,13,63,23,28', 'pneumonia'),
(6, '12,22,16,3,63,13,17,5,14', 'typhoid'),
(7, '30,19,36,11,18,39,15,31,32', 'diarrhea'),
(8, '1,11,3,66,37,22,23', 'measles'),
(9, '40,41,42', 'ringworm'),
(10, '42,43,44', 'scabies'),
(11, '48,49,50,51,52,67,68,53', 'pink eye'),
(12, '11,23,5,3,63,1,69,15,14,22', 'influenza'),
(13, '13,22,11,24,63,18,43', 'hepatitis A'),
(14, '10,58,56,57', 'hearing loss'),
(15, '3,4,1,11,14', 'whooping cough'),
(16, '22,62,63,25,64,45,26', 'anemia'),
(17, '12,3,71,29,47', 'Respiratory Syncytial Virus (RSV)'),
(18, '12,62,29,30,54,18', 'sepsis'),
(19, '33,34,46,72,38,11,17,42,27', 'jaundice'),
(20, '59,61,55,63,60,19,70,65,36,5', 'sleep apnea'),
(21, '', 'invalid');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
