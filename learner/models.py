from django.db import models

# Create your models here.
class trainDB(models.Model):
	symptoms = models.TextField()
	disease = models.IntegerField()


class symptomsDB(models.Model):
	symptom = models.CharField(max_length=100)
	alias = models.CharField(max_length=100)


class diseaseDB(models.Model):
	symptoms = models.TextField()
	disease = models.CharField(max_length=100)

class classifyDB(models.Model):
	symptoms = models.TextField()
	head = models.CharField(max_length=100)


class infoDB(models.Model):
	diseaseid = models.IntegerField()
	intro = models.TextField()
	cause = models.TextField()
	symptoms = models.TextField()
	preventtion = models.TextField()
	treatment = models.TextField()
	seedoc = models.TextField()
	

