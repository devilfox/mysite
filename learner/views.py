from django.shortcuts import render
from django.http import HttpResponse
import learner.custom as custom;
from learner.models import *
#

# Create your views here.
def index(request):
	return HttpResponse("Hello world.yeah")

def classify(request,symptoms):
	symptoms = symptoms.split('(')[1].split(')')[0].split(',')
	result = custom.cusclassify(symptoms)
	return HttpResponse(result)	
 

def add(request,details):
	details = details.split('(')[1].split(')')[0].split('/')
	symptoms = map(int,details[0].split(','))
	disease = int(details[1])
	custom.pushNewTrainData(symptoms,disease)
	return HttpResponse("done")
	

def getlist(request):
	import learner.custom as lst
	labels = lst.getlabels()
	returnlist = '('
	for label in labels:
		returnlist+=label+','
	returnlist = returnlist[:-1]
	returnlist+=')'
	return HttpResponse(returnlist)

def classifiedlist(request):
	return HttpResponse(custom.classifiedlist())


def getdiseaselist(request):
	diseaselst = custom.getdiseaselist()
	returnlst = '('
	for disease in diseaselst:
		returnlst+=disease+','
	returnlst = returnlst[:-1]+')'
	return HttpResponse(returnlst)



def train(request):
	data,labels = custom.createDataSet()
	Tree = custom.createTree(data,labels)
	custom.saveTree(Tree)
	return HttpResponse('done')

def rank(request,symptoms):
	symptoms = symptoms.split('(')[1].split(')')[0].split(',')
	result = custom.rank(symptoms)
	return HttpResponse(result)	

def getIntro(request,Disease):
	Id = int(Disease.split('(')[1].split(')')[0])
	info = infoDB.objects.filter(diseaseid=Id)[0].intro
	return HttpResponse(info)

def getCause(request,Disease):
	Id = int(Disease.split('(')[1].split(')')[0])
	info = infoDB.objects.filter(diseaseid=Id)[0].cause
	return HttpResponse(info)

def getSymptoms(request,Disease):
	Id = int(Disease.split('(')[1].split(')')[0])
	info = infoDB.objects.filter(diseaseid=Id)[0].symptoms
	return HttpResponse(info)

def getPrevention(request,Disease):
	Id = int(Disease.split('(')[1].split(')')[0])
	info = infoDB.objects.filter(diseaseid=Id)[0].preventtion
	return HttpResponse(info)

def getTreatment(request,Disease):
	Id = int(Disease.split('(')[1].split(')')[0])
	info = infoDB.objects.filter(diseaseid=Id)[0].treatment
	return HttpResponse(info)

def getSeedoc(request,Disease):
	Id = int(Disease.split('(')[1].split(')')[0])
	info = infoDB.objects.filter(diseaseid=Id)[0].seedoc
	return HttpResponse(info)