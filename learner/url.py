from django.conf.urls import patterns, include, url
from learner import views

urlpatterns = patterns('',
 url(r'^(?P<symptoms>.[() a-zA-Z0-9,]+)/$', views.classify, name='classify'),
)