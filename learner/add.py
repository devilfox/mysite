from django.conf.urls import patterns, include, url
from learner import views

urlpatterns = patterns('',
 url(r'^$',views.index,name='index'),
 url(r'^(?P<details>.[() a-zA-Z0-9,|/]+)/$', views.add, name='add'),
)