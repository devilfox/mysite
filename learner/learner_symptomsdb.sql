-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 12, 2014 at 08:26 AM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_myguide`
--

-- --------------------------------------------------------

--
-- Table structure for table `learner_symptomsdb`
--

CREATE TABLE IF NOT EXISTS `learner_symptomsdb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symptom` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `learner_symptomsdb`
--

INSERT INTO `learner_symptomsdb` (`id`, `symptom`, `alias`) VALUES
(1, 'runny nose', ''),
(2, 'nasal discharge', ''),
(3, 'coughing', ''),
(4, 'sneezing', ''),
(5, 'sore throat', ''),
(6, 'sinus infection', ''),
(7, 'pulls or grabs ear', ''),
(8, 'unpleasant ear smell', ''),
(9, 'swelling around nose', ''),
(10, 'startled by loud noise', ''),
(11, 'mild fever', ''),
(12, 'severe fever', ''),
(13, 'loss of appetite', ''),
(14, 'diarrhea', ''),
(15, 'vomiting', ''),
(16, 'constipation', ''),
(17, 'stomach ache', ''),
(18, 'abdominal pain', ''),
(19, 'irritability', ''),
(20, 'difficulty sleeping', ''),
(21, 'crying', ''),
(22, 'weakness', ''),
(23, 'muscle pain', ''),
(24, 'joint pain', ''),
(25, 'difficulty concentrating', ''),
(26, 'leg cramps', ''),
(27, 'weight loss', ''),
(28, 'trouble breathing', ''),
(29, 'rapid breathing', ''),
(30, 'less frequent urination', ''),
(31, 'loose stools', ''),
(32, 'frequent poops', ''),
(33, 'dark urine', ''),
(34, 'pale stool', ''),
(35, 'jaw pain', ''),
(36, 'dry mouth', ''),
(37, 'white spots in mouth', ''),
(38, 'yellowing in mucous membrane', ''),
(39, 'blood loss', ''),
(40, 'scaly patches', ''),
(41, 'hair loss in patches', ''),
(42, 'itchy skin', ''),
(43, 'rashes', ''),
(44, 'pimpy read humps', ''),
(45, 'pale skin', ''),
(46, 'yellow skin', ''),
(47, 'bluish skin', ''),
(48, 'readness in white part of eye', ''),
(49, 'increased tears amount', ''),
(50, 'yellow discharge crusts over eyelashes', ''),
(51, 'green or white eye discharge', ''),
(52, 'blurred vision', ''),
(53, 'increased sensitivity in light', ''),
(54, 'abrupt change in mental status', ''),
(55, 'rapidly falling asleep', ''),
(56, 'slowy learn to talk', ''),
(57, 'talkes loudly', ''),
(58, 'only responds to visual clues not sound', ''),
(59, 'pauses while snoring', ''),
(60, 'memory or learning problems', ''),
(61, 'fighting sleepliness during day', ''),
(62, 'rapid heart beat', ''),
(63, 'headache', ''),
(64, 'diziness', ''),
(65, 'walking frequently to urinate', ''),
(66, 'sore eyes', ''),
(67, 'itchy eye', ''),
(68, 'burning eyes', ''),
(69, 'nausea', ''),
(70, 'depressed', ''),
(71, 'weezing', ''),
(72, 'white eyes', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
