from math import log
import operator

def calculateShannonEntropy(dataSet):
 numEntries = len(dataSet)
 labelCounts = {}
 for featureVector in dataSet:
  currentLabel = featureVector[-1]
  if currentLabel not in labelCounts.keys():
   labelCounts[currentLabel] = 0
  labelCounts[currentLabel] += 1
 shannonEnt = 0.0
 for key in labelCounts:
  prob = float(labelCounts[key])/numEntries
  shannonEnt -= prob * log(prob,2)
 return shannonEnt


#def createDataSet():
# dataSet = [[1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 'common cold'],
#			      [0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 'ear infection'],
#			      [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 'chickenpox'],
#			      [0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 'common cold'],
#			      [1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 'common cold'],
#            [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 'chickenpox'],
#            [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 'ear infection'],
#            [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 'chickenpox'],
#            [0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 'ear infection'],
#            [1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 'common cold']
#			]
# labels = ['sneezing','runny nose','sore throat','pulling ears','excessive crying','sleep disturbance','cough','headache','nasal congestion','fever',] 
# return dataSet, labels


def splitDataSet(dataSet, axis, value):
 retDataSet = []
 for featureVector in dataSet:
  if featureVector[axis] == value:
   reducedfeatureVector = featureVector[:axis]
   reducedfeatureVector.extend(featureVector[axis+1:])
   retDataSet.append(reducedfeatureVector)
 return retDataSet

def chooseBestFeatureToSplit(dataSet):
 numFeatures = len(dataSet[0]) - 1
 baseEntropy = calculateShannonEntropy(dataSet)
 bestInfoGain = 0.0; bestFeature = -1
 for i in range(numFeatures):
  featureList = [example[i] for example in dataSet]
  uniqueVals = set(featureList)
  newEntropy = 0.0
  for value in uniqueVals:
   subDataSet = splitDataSet(dataSet, i, value)
   prob = len(subDataSet)/float(len(dataSet))
   newEntropy += prob * calculateShannonEntropy(subDataSet)
  infoGain = baseEntropy - newEntropy
  if (infoGain > bestInfoGain):
   bestInfoGain = infoGain
   bestFeature = i
 return bestFeature


def majorityCount(classList):
 classCount={}
 for vote in classList:
  if vote not in classCount.keys(): classCount[vote] = 0
  classCount[vote] += 1
 sortedClassCount = sorted(classCount.iteritems(),key=operator.itemgetter(1), reverse=True)
 return sortedClassCount[0][0]


def createTree(dataSet,labels):
 classList = [example[-1] for example in dataSet]
 if classList.count(classList[0]) == len(classList):
  return classList[0]
 if len(dataSet[0]) == 1:
  return majorityCount(classList)
 bestFeat = chooseBestFeatureToSplit(dataSet)
 bestFeatureLabel = labels[bestFeat]
 myTree = {bestFeatureLabel:{}}
 del(labels[bestFeat])
 featureValues = [example[bestFeat] for example in dataSet]
 uniqueVals = set(featureValues)
 for value in uniqueVals:
  subLabels = labels[:]
  myTree[bestFeatureLabel][value] = createTree(splitDataSet(dataSet, bestFeat, value),subLabels)
 return myTree



def classify(inputTree,featureLables,testVec):
 firstStr = inputTree.keys()[0]
 secondDict = inputTree[firstStr]
 featIndex = featureLables.index(firstStr)
 for key in secondDict.keys():
  if testVec[featIndex] == key:
   if type(secondDict[key]).__name__=='dict':
    classLabel = classify(secondDict[key],featureLables,testVec)
   else:
    classLabel = secondDict[key]
 return classLabel

def saveTree(Tree):
 import pickle
 fw = open('learner/tree','w')
 pickle.dump(Tree,fw)
 fw.close()

def retriveTree():
 import pickle
 fr = open('learner/tree')
 return pickle.load(fr)